/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package co.verisoft.fw.utils.internal;

public final class Sel4Junit5ProjectConstants {

    public static final String ROOT_PROPERTY_PATH = "./src/main/resources/sel4jup5.properties";
    public static final String SELENIUM_LOGS_VERBOSE = "selenium.logs.verbose";
    public static final String SELENIUM_WAIT_TIMEOUT = "selenium.wait.timeout";
    public static final String POLLING_INTERVAL = "polling.interval";
    public static final String MAX_RETRY_NUMBER = "max.retry.number";
    public static final String LOG4J2_CONFIG_PATH = "log4j2.config.path";
    public static final String MOBILE_CONFIG_PATH = "mobile.config.path";
    public static final String CHANGE_REPORT_ICONS = "change.report.icons";
}
